{-#LANGUAGE ScopedTypeVariables#-}

module Main where
import Control.Monad

import Game

main = do
  putStr "Choose type of the game: Player to Player (1) or Player to Computer (2): "
  typeOfGame :: Int <- readLn
  if (typeOfGame == 1) then 
    do
      putStrLn "Chosen type of the game: Player to Player"
      playP2P
  else if (typeOfGame == 2) then
         do
           putStrLn "Chosen type of the game: Player to Computer"
           playP2C
       else do
              putStrLn "Please input 1 or 2"
              main
