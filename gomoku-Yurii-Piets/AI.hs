module AI where

import Types
import Analyze

depth :: Int
depth = 1

data GameTree = Leaf {val::Int, world::World} | Node {world::World, bestLeaf :: GameTree}

betterWorld :: World -> World
betterWorld world@(World board move _ ) = (World (bestBoard tree) (nextMove move) winner)
  where
    tree = buildTree world depth
    bestBoard (Node _ leaf) = bestBoard  leaf
    bestBoard (Leaf _ (World b _ _)) = b
    winner = if (checkWinner (World (bestBoard tree) move Nothing) av) then Just move else Nothing


--drzewo zapamiętuje tylko wartosc "najlepszej tablicy",
--ponieważ w przyapku gdy są zapamiętuwane wszystkie możliwości dostahe się Error: stack overflow
buildTree :: World -> Int -> GameTree
buildTree world@(World board move _) depth 
  | depth < 1 = undefined
  | depth == 1 = genLeaf world
  | depth `mod` 2 == 1 = (Node world (getMin childNodes))
  | depth `mod` 2 == 0 = (Node world (getMax childNodes))
      where
        childNodes = go av
        go [] = []
        go (x:xs) = go xs ++ [buildTree newWorld (depth-1)]
          where 
            newWorld = World newBoard (nextMove move) Nothing
            newBoard = insertBoard x move board

genLeaf :: World -> GameTree
genLeaf world@(World board move _) = Node world (getMax (childLeafs world (allNeighbours board)))

childLeafs world@(World board move _) [] = []
childLeafs world@(World board move _) (x:xs) = listLeafs ++ if (elem leaf listLeafs) then [] else [leaf]
  where
    listLeafs = childLeafs world xs
    leaf = initLeaf newWorld
    newWorld = World newBoard move Nothing
    newBoard = insertBoard x move board

getMax :: [GameTree] -> GameTree
getMax [x] = x
getMax (x:xs) = if (getMax xs > x) then getMax xs else x

getMin :: [GameTree] -> GameTree
getMin [x] = x
getMin (x:xs) = if (getMin xs < x) then getMin xs else x

evalWorld :: World -> Int
evalWorld world@(World board move _)
  | checkX (World board (nextMove move) Nothing) av 5 = -5000
  | checkX (World board (nextMove move) Nothing) av 4 = -300
  | checkX (World board (nextMove move) Nothing) av 3 = -100
  | checkX (World board (nextMove move) Nothing) av 2 = -20
  | checkX (World board (nextMove move) Nothing) av 1 = -1
  | checkX world av 5 = 5000
  | checkX world av 4 = 300
  | checkX world av 3 = 100
  | checkX world av 2 = 20
  | checkX world av 1 = 1
  | otherwise = 0

instance Show GameTree where
  show (Leaf val world) = "Value: " ++ show val ++ "\n" ++ show world
  show (Node world leaf) = show world ++ show leaf

instance Eq GameTree where
  (==) (Leaf v1 _) (Leaf v2 _) = v1 == v2
  (==) (Node _  v1) (Node _  v2) = v1 == v2

instance Ord GameTree where
  (>) (Leaf v1 _) (Leaf v2 _) = v1 > v2
  (>) (Node _ v1) (Node _ v2) = v1 > v2
  (<) (Leaf v1 _) (Leaf v2 _) = v1 < v2
  (<) (Node _ v1) (Node _ v2) = v1 < v2


allNeighbours :: Board -> [Position]
allNeighbours board = rnf $ removeDuplicates $ go av
  where
    rnf [] = []
    rnf (x:xs) = rnf xs ++ if (isFree x board) then [x] else []
    go [] = []
    go (x:xs) = go xs ++ if (isFree x board) then [] else fiveAllDirection x

initLeaf :: World -> GameTree
initLeaf world = Leaf (evalWorld world) world
