module Analyze where
import Types
import Data.HashMap.Strict

analyzeAllDirection :: Board -> Color -> Position -> Bool
analyzeAllDirection board color position =
  (analyze board color $ fiveInRow position)
  || (analyze board color $ fiveInCol position)
  || (analyze board color $ fiveInDiagLeftToRight position)
  || (analyze board color $ fiveInDiagRightToLeft position)

analyze :: Board -> Color -> [Position] -> Bool
analyze board expectedColor direction
  | length direction == 0 = True
  | otherwise = (same expectedColor (Data.HashMap.Strict.lookup (head direction) (getMap board))) &&
                (analyze board expectedColor (tail direction))
    where getMap (Board map) = map
          same expected current = case current of 
            Just c -> c == expected
            Nothing -> False
    
fiveInRow :: Position -> [Position]
fiveInRow (x, y) = zip [x-2 .. x+2] $ repeat y

fiveInCol:: Position -> [Position]
fiveInCol (x, y) = zip (repeat  x) [y-2 .. y+2]

fiveInDiagLeftToRight :: Position -> [Position]
fiveInDiagLeftToRight (x, y) = zip [x-2 .. x+2] [y-2 .. y+2]

fiveInDiagRightToLeft :: Position -> [Position]
fiveInDiagRightToLeft (x, y) = zip [x-2 .. x+2] $ reverse [y-2 .. y+2]

xInRow :: Int ->  Position -> [Position]
xInRow v (x, y) = zip [x .. x + v - 1] $ repeat y

xInCol:: Int -> Position -> [Position]
xInCol v (x, y) = zip (repeat  x) [y .. y + v - 1]

xInDiagLeftToRight :: Int -> Position -> [Position]
xInDiagLeftToRight v (x, y) = zip [x .. x + v - 1] [y .. y + v - 1]

xDiagRightToLeft :: Int ->  Position -> [Position]
xDiagRightToLeft v (x, y) = zip [x .. x + v - 1] $ reverse [y .. y + v - 1]

fiveAllDirection :: Position -> [Position] 
fiveAllDirection pos = removeItem pos (removeNegative $ removeDuplicates $ (fiveInRow pos) ++ (fiveInCol pos) ++ (fiveInDiagLeftToRight pos) ++ (fiveInDiagRightToLeft pos))

av :: [Position]
av = [(x,y) | y <- [1..dimension], x <- [1..dimension]]

nextMove :: Color -> Color
nextMove color = case color of
  Black -> White
  White -> Black

checkWinner :: World -> [Position] -> Bool
checkWinner world@(World board move _) arr
  | length arr == 0 = False
  | analyzeAllDirection board move h = True
  | otherwise = checkWinner world t
    where
      h = head arr
      t = tail arr

checkX :: World -> [Position] -> Int ->  Bool
checkX world@(World board move _) arr x 
  | length arr == 0 = False
  | analyzeXAllDirection board move h x = True
  | otherwise = checkX world t x
    where
      h = head arr
      t = tail arr

isWinner :: Color -> World -> Bool
isWinner expectedColor (World _ _ winner) = case winner of
  Just currentColor -> expectedColor == currentColor
  Nothing -> False 

isInRange :: Position -> Bool
isInRange (x, y) = not $ x < 1 || x > 19 || y < 1 || y > 19

analyzeXAllDirection :: Board -> Color -> Position -> Int -> Bool
analyzeXAllDirection board color position x =
  (analyze board color $ xInRow x position)
  || (analyze board color $ xInCol x position)
  || (analyze board color $ xInDiagLeftToRight x position)
  || (analyze board color $ xDiagRightToLeft x position)

removeDuplicates :: Eq a => [a] -> [a]
removeDuplicates = rdHelper []
    where rdHelper seen [] = seen
          rdHelper seen (x:xs)
              | x `elem` seen = rdHelper seen xs
              | otherwise = rdHelper (seen ++ [x]) xs

removeItem _ [] = []
removeItem x (y:ys) | x == y    = removeItem x ys
                    | otherwise = y : removeItem x ys

removeNegative :: [Position] -> [Position] 
removeNegative [] = []
removeNegative list@(x:xs) = removeNegative xs ++ if (isInRange x) then [x] else []
