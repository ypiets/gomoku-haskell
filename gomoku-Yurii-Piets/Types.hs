module Types where 
import Data.HashMap.Strict

data Color = Black | White deriving Eq
instance Show Color where
  show color = case color of
    Black -> "*"
    White -> "o"

type Position = (Int,Int)
data Board = Board (HashMap Position Color)

data World = World { board::Board, move::Color, winner::Maybe Color}

instance Show Board where
  show board = toStringMatrix board avPos

instance Show World where
  show (World board move winner) = (printMove move) ++ "\n" ++ (printWinner winner) ++ "\n" ++ (show board)
    where printMove move = case move of 
            White -> "White is moving"
            Black -> "Black is moving"
          printWinner winner = case winner of
            Just White -> "White is a winner!"
            Just Black -> "Black is a winner!"
            Nothing -> "No current winner"

avPos :: [[Position]]
avPos = [[ (x,y) | x <- [1..dimension]] | y <- [1..dimension]];

dimension :: Int
dimension = 19

toStringRow :: Board -> [Position] -> String
toStringRow board array
  | length array == 0 = ""
  | otherwise = (showOne (head array)) ++ "|" ++ (toStringRow board (tail array))
      where
        showOne pos = go (Data.HashMap.Strict.lookup pos (getMap board))
        getMap (Board board) = board
        go Nothing = " "
        go (Just color) = show color

toStringMatrix :: Board-> [[Position]] -> String
toStringMatrix board matrix
  | length matrix == 0 = ""
  | otherwise = "|" ++ (toStringRow board (head matrix)) ++ "\n" ++ (toStringMatrix board (tail matrix))

initBoard::Board
initBoard = Board empty

insertBoard :: Position -> Color -> Board -> Board
insertBoard position color (Board map) = Board $ insert position color map

isFree :: Position -> Board -> Bool
isFree position (Board map) = not $ member position map

initWorld :: World
initWorld = World initBoard White Nothing
