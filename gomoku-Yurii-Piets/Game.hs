{-#LANGUAGE ScopedTypeVariables#-}
module Game where

import Types
import Analyze
import AI

--player with player--
playP2P = do
  putStrLn "Game Player to player"
  stepP2P initWorld

stepP2P world@(World board move winner) = case winner of
  Just _ -> putStrLn $ show world
  Nothing -> do
    putStrLn $ show world
    position <- getMove board
    stepP2P $ newWorld position
      where
        newWorld position = World (newBoard position) (nextMove move) (newWinner position)
        newBoard position = (insertBoard position move board)
        newWinner position = if (checkWinner (World (newBoard position) move Nothing) av)
                               then (Just move)
                               else Nothing
--player with player--

--player with computer--
playP2C = do
  putStrLn "Game with computer"
  playerStep initWorld

playerStep world@(World board move winner) = case winner of
  Just _ -> putStrLn $ show world
  Nothing -> do
    putStrLn $ show world
    position <- getMove board
    computerStep $ newWorld position
      where
        newWorld position = World (newBoard position) (nextMove move) (newWinner position)
        newBoard position = (insertBoard position move board)
        newWinner position = if (checkWinner (World (newBoard position) move Nothing) av)
                               then (Just move)
                               else Nothing


computerStep world@(World board move winner) = case winner of
  Just _ -> putStrLn $ show world
  Nothing -> do
    putStrLn $ show world
    playerStep $ AI.betterWorld world
--player with computer--

getMove board = do
  putStr "x="
  x :: Int <- readLn
  putStr "y="
  y :: Int <- readLn
  if (isInRange (x,y) && isFree (x,y) board) then 
    return (x,y)
  else do
         putStrLn "Must be in a numer in range [1..19]"
         getMove board
